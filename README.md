# ![Icon](icon.png) CLOCK.PY

clock.py is a lightweight, expandable clock, alarm clock, timer and network device monitor
for the terminal. It is written in pure [Python](https://python.org) for the Linux CLI. So far it
works fine in any terminal application/emulator and is a perfect companion for the
excellent [tmux](https://github.com/tmux/tmux/wiki) terminal multiplexer (or screen etc.).

![Screenshot](screenshot.png)

*This is, what *clock.py* can look like with a set alarm, running timer and the network monitor
in curly brackets in the top line. Only one machine is online in this example.*

The color can be changed to one of the main (bright) terminal colors (press '#' to do that).

The ASCII art fonts can be toggled between large and small for any line,
the network monitor's target (in the top line) can be edited from within the running app, timers and alarms can
be set from within the app.

All items can be toggled between two states (compact/invisible and visible). In compact view,
the same clock from above can look like this, without loosing functionality:

![Screenshot](screenshot_compact.png)
*All elements in compact view, 8 minutes later*

## Dependencies
clock.py relies on the included helper libraries called pydpyl and pydfl and uses the
standard python modules
- multiprocessing
- subprocess
- datetime
- gc
- os
- sys
- time
- signal
- configparser
from pydpyl:
- traceback
- select

## Modules

Currently, clock.py provides these modules (information providers)
clock - the current time
alarm - set dayly (audible) alarms
timer - set (audible) countdown alarms
gong - audible quarter-hourly old fashioned gong
netstat - network device monitor (via ping)

### Helper Module
beeper, color and player are helper modules, used to play wave files and
set the color of the clock

## Usage
At first start, clock.py creates a basic configuration file (~/.config/clock.ini).

A list of standard hotkeys can be shown by pressing the h-key:

    Hotkeys:
     --  clock.py (v2.0.2)  --  help  --
    #     change color
    b     toggle audible alarm
    c/C   toggle clock/change clock's font size
    f     show/hide seconds
    n     toggle network status check
    N     edit network status targets
    a/A   toggle alarm/change font size
    sa/xa set/delete alarm
    t/T   toggle timer/change timer font size
    st/xt set/delete timer
    g     toggle audible quarter hourly gong

*clock.py uses 'double hotkeys', press s-t to set the timer. These are the standard
hotkeys. The exact hotkeys may vary, depending on the confirguration you are using. The help screen
will dynamically adjust to your changes.*


## Module API
An information provider module must or may have the following methods.

    .vis(bool)

  this is called when a modules visibility changes
  this method is called once at clock.py start to ensure proper initialization
  
    .pass_key(char)

  this function receives a single character and is called to pass the other hotkeys
  except the visibility/default hotkey
  
    .__del__()

  can be used to allow for proper module deletion on exiting clock.py

## Portability
The main issues when porting this to Windows OS would be the handling of the stdin/stdout
to suppress messages and to have a non-blocking keyboard input.

## References and 3rd party assets
The project uses these royalty free sounds from pixabay.com:
- Service Bell Ring from pixabay: https://pixabay.com/sound-effects/service-bell-ring-14610/, downloaded on 2023-12-04
- Gong1 from pixabay: https://pixabay.com/sound-effects/gong1-94016/, downloaded on 2023-12-04
