#!/bin/python
"""
FONTS, BANNERS and BANNER PRINTING for python

This package provides basic ASCII-font handling and two functions
banner(text, fontnr) and bannerprint(text, fontnr).

banner(text, fontnr) returns several lines in a list format that represent
  the given text in ASCII-large code.

bannerprint(text, fontnr) prints the text in large ASCII font.

version 1.1 (2016) by docphees (software@phees.net)
license: GPL

CHANGELOG:
v1.1
- readability changes on minimal font
"""


class Font(object):
    """
    The Font class provides a font object.
    """

    def __init__(self, name="", content="", width=0, height=0):
        self.name = name
        self.content = content
        self.width = width
        self.height = height
        self.data = []

    def add(self, line):
        self.data.append(line)


def banner(line="", fontnr=0):
    """The BANNER function
    provides large ASCII printing
    """
    chars = []
    fonts = buildfonts()
    for c in line:
        #print(c, ":", fonts[font].content.find(c))
        chars.append(fonts[fontnr].content.find(c))
    #print(chars)
    #print("Font's name:", fonts[fontnr].name)
    textlines = []
    for n in range(0, fonts[fontnr].height):
        textline = ""
        for i in chars:
            textline += " " + fonts[fontnr].data[n][i]
        textlines.append(textline)
    return textlines


def bannerprint(line="", fontnr=0):
    textlines = banner(line, fontnr)
    for line in textlines:
        print(line)


def buildfonts():
    fonts = []

    fontdef = Font("One", "0123456789:. ", 6, 6)
    fontdef.add([" ##### ", "   #   ", " ##### ", " ##### ", "#      ", " ######", " ##### ", "#######", " ##### ", " ##### ", "   ", "   ", "   "])
    fontdef.add(["#     #", "  ##   ", "#     #", "#     #", " #     ", " #     ", "#      ", "#    # ", "#     #", "#     #", " o ", "   ", "   "])
    fontdef.add(["#     #", " # #   ", "    #  ", "    ## ", " # #   ", " ##### ", "###### ", "    #  ", " ##### ", " ######", "   ", "   ", "   "])
    fontdef.add(["#     #", "   #   ", "  #    ", "      #", "#######", "      #", "#     #", " ##### ", "#     #", "      #", " o ", "   ", "   "])
    fontdef.add(["#     #", "   #   ", " #     ", "#     #", "   #   ", "#     #", "#     #", "   #   ", "#     #", "      #", "   ", "   ", "   "])
    fontdef.add([" ##### ", " ##### ", "#######", " ##### ", "   #   ", " ##### ", " ##### ", "   #   ", " ##### ", " ##### ", "   ", " o ", "   "])
    fonts.append(fontdef)

    fontdef = Font("beauty", "0123456789:. ", 6, 6)
    fontdef.add([" ----- ", "   ,   ", " .---. ", " .---. ", "  .    ", "-----  ", "  .--. ", ".----- ", " .---. ", " .--.  ", "   ", "   ", "   "])
    fontdef.add(["|    /|", "  /|   ", "     | ", "      |", " /     ", "|      ", " /     ", "     / ", "(     )", "|    \ ", " o ", "   ", "   "])
    fontdef.add(["|   / |", " / |   ", "    /  ", "    -- ", "/  |   ", "`---.  ", "|----. ", "    /  ", " :---: ", " '----|", "   ", "   ", "   "])
    fontdef.add(["|  /  |", "   |   ", "   /   ", "      |", "---+---", "     | ", "|     |", "  -+-  ", "/     \\","      |", " o ", "   ", "   "])
    fontdef.add(["| /   |", "   |   ", "  /    ", "      |", "   |   ", "     | ", "|     |", "   |   ", "\     /", "     / ", "   ", "   ", "   "])
    fontdef.add([" ----- ", "  ---  ", " ----- ", " '---' ", "   |   ", "'---'  ", " '---' ", "   |   ", " '---' ", " ---'  ", "   ", " o ", "   "])
    fonts.append(fontdef)

    fontdef = Font("compact", "0123456789:. ", 3, 3)
    fontdef.add([".-.", " . ", ".-,", " -,", " / ", "---", ",-.", "___", ".-.", ".-.", " ", " ", " "])
    fontdef.add(["| |", " | ", " / ", " -|", "'+-", "'-,", "|-.", "  /", ":-:", "'-|", ":", " ", " "])
    fontdef.add(["'-'", " ' ", "---", " -'", " | ", "'-'", "'-'", " / ", "'-'", " -'", " ", ".", " "])
    fonts.append(fontdef)

    fontdef = Font("minimal", "0123456789:. ", 2, 2)
    fontdef.add(["/\\", " |", "')", "')", "L|", "|'", " /", "'/", "()", "()", ".", " ", " "])
    fontdef.add(["\/",  " |", "/,", ".)", " |", ",)", "()", "/ ", "()", "/ ", "'", ".", " "])
    fonts.append(fontdef)
    return fonts

if __name__ == "__main__":
    print("A quick demo of fonts.py's capabilities:")
    print('fonts.bannerprint("13:56:12", 0)')
    bannerprint("13:56:12", 0)
    print('fonts.bannerprint("1234567890", 1)')
    bannerprint("1234567890", 1)
    print('fonts.bannerprint("13:58:19", 2)')
    bannerprint("13:58:19", 2)
    print('fontsbannerprint("13:58:19", 3)')
    bannerprint("13:58:19", 3)