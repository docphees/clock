#!/bin/python
"""
CLOCK
# A proper rewrite of clock.py
# Using an InformationProviderObject/Assembly framework, allowing for easy expansion
# and individualisation.
"""

## TODOS #############################################################################
# TODO Move vis_handling into the ipo objects to make the state visible to them and allow to toggle
#  themselves on and off!
# TODO clock IPO should have an optional offset, allowing for a secondary
#  clock in another time zone
# TODO make IPOs register hotkeys themselves
#  - give the IPO an additional hotkey setting (defaulting!) and after loading the
#    configuration, have a register_hotkey round with the IPOs
#  - hotkeys should be allowed to be set via clock.ini, so that IPOs ca/home/petov/PycharmProjects/clock.localn
#    be used more than once - (two timers for example)
# TODO let IPOs trigger settings saving (with an optional check_save() IPO API?)
#  check if IPO has check_save(), if so, run it?
# TODO implement pydpyl debugging
#  - library is loaded anyway, let's use it
#  - alternatively switch to python's logging module (as in dirwatch)
# TODO fix an always active netstat worker processes
#  - they still seem to always run (or at least once), even if not visible from
#    the beginning! (or deactivated in between)
# TODO New IPOs to write:
#  - weather forecast (https://www.w3schools.com/charsets/ref_utf_symbols.asp)
#    based on wttr.in
#  - add a setup menu in Netstat style to enter a city/location or even auto-location/geoclue
# TODO restructure IPO library, split up into one file per IPO and auto-import as needed.
#  - IPO-files could be put into a .local/share/clock.py/ipos directory and be updated/pulled from the web
#  - possibly
# TODO add histogram option to network viewer / netstat?
#  - show instead of the main window for a few seconds. Toggle histograms on/off with 1-0 keys.

# DONES
# TODOx added bell character fallback to alarm / playsound
# TODOx Remove libreoffice from default network monitor setup
# TODOx Netstat IPO should allow empties ("-" or " ") for grouping of IP targets.
#   Nearly all characters are allowed, except backslash and double quotes.
# TODOx REGRESSION: Network monitor editing/entry of ips does not recognise
#   return and backspace (\r and \n)
# TODOx only import used IPOs (no)
# TODOx collect and print help texts from all modules
# - in progress! collection and printing is currently missing
# TODOx hotkeys cannot have multiple receivers
# - Alarm and Timer IPOs should all receive the space-hotkey for silencing.
# - hotkeys must be switched to a list object instead of dictionary [ [key, ipo], [key, ipo]...]
# - this must be solved in IAssembler!
# TODOx gettext() might return an array of tuples:
# - NO. APOs are agnostic to the InformationAssembler! The following things would break that!
# - [ ["main":text], ["statusbar":othertext] ]
# - this allows modules to write to several locations
# - this (or another solution) is needed for the audible alarm indicator in the statusbar
# - maybe this can be delegated to an alarm IP?
# - Buuut: This makes the IP gnostic of the main()! Without, the output does not have to know its location!
# - Maybe: location can be array ["loc1", "loc2"]. if the output is an array, it will be send to the according locations
# TODOx Make the color switching an IPO
# TODOx saving/loading of settings:
# - decision to go with configparser library
# - collect all saveable data from the information providers (API: get_saveable_data(), setup(data))
#   and from the Assembly object
# - evaluate if the whole objects can be saved? (untidy, but possible)
#   xTODO settings have to be sanitized by the IPs themselves. maybe sanitize all of them centrally?
#   xTODO settings format: Dictionary of dictionaries?
#      - NO: lets use the cfg save file for plugin handling as well.
#            - save the ips with their settings and connect them with their own settings by ID or name?
#      - YES: but this gets complicated soon.
# TODOx vis-hotkeys not working! Regression. (compare input_key_old)
# - the "main" hotkey has been made unresponsive by the new multi-key-hotkey solution. a second hotkey-run must be
#   done to check for primary hotkeys.
# TODOx input of data (set alarm, countdown etc)
# - current multi-key-hotkeys are freely defined.
# - this could be setup as hotkey "sa". main() waits after s for one second for another character. is not entered,
#   main() proceeds. if so, hotkey is passed to the ipo.
# - this could be more plugin-friendly if each ip-modules' hotkey would be preceded by one per-ipo-hotkey.
#   all alarm ("a") hotkeys would be "a-s", "c-x" etc.
#   but this can be done at ipo-loading, let's keep it open.
#   Multi-key-hotkeys is closed.


from ipos import *
import pydpyl as pdp

import signal
import configparser

version = pdp.Version(2, 1, 0)

#########
# ASSEMBLER OF INFORMATION PROVIDERS' TEXT OUTPUTS
class IAssembler:
    """
    Information Assembler

    This assembles all the information providers (clock, netstat, alarm, countdown...),
    handles hotkey management and sends information to the proper output locations
    (statusbar, mainarea, infoline)

    TODO Make output elements objects of type linear or area instead of hardcoded ones
        - Create your own clock screen with that.

    information providers will be set up as an array of dictionaries:
    [ {name:"netstat", obj:netstat, pollfct:netstat.gettext, location:"statusbar_inside", vis:False, hotkeys:["a","bs","c"] }, {... ]
    the first hotkey is always the visibility toggle. this can be None, making the ip permanent!
    this data model is easily expandable

    TODO Document the API for information providers.
    handling visibility, output etc. itself?
    Every ip should be at least a simple wrapper anyway. do not let unfiltered data stream into this!
    This would also make hotkey handling simpler.
    Create a simple wrapper example IP

    current possible locations:
    "main", "statusbar", "before_statusbar", "after_statusbar", "infoline"

    ipos will be dealt with in the order of the ipo list! Alarm and gong ips should be
    last in the list to make sure all alarm stati are properly evaluated.
    """

    def __init__(self):
        # set path to configuration file
        self.config_path = os.path.expanduser("~/.config/clock.ini")
        # setup information providers
        self.ipos = []

        # setup all information providers
        self.setup_ipos( self.load_settings() )

        # create array of hotkeys
        self.hotkeys = []
        self.primary_hotkeys = []
        self.refresh_hotkeys()

        for ipo in self.ipos:             # make sure the objects know about their visibility!
            ipo["obj"].vis(ipo["vis"])

    def setup_ipos(self, data={}):
        if len(data) == 0:
            # If settings is empty, build a default clock
            self.add_ipo("color", "Color", "gettext", "before_statusbar", None, True, [None, "#"])

            self.add_ipo("clock", "Clock", "gettext", "main", "statusbar", True, ["c", "C", "f"])

            self.add_ipo("netstat", "Netstat", "gettext", "after_statusbar", None, True, ["n", "N"])

            self.add_ipo("alarm", "Alarm", "gettext", "main", "infoline", True, ["a", "A", "sa", "xa", " "])

            self.add_ipo("timer", "Timer", "gettext", "main", "infoline", True, ["t", "T", "st", "xt", " "])

            self.add_ipo("beeper", "Beeper", "gettext", "statusbar", None, True, [None, "b"])
            # let the Beeper IPO handle audible alarms for the Alarm and Timer Objects:
            self.ipos[-1]['obj'].set_settings({"providerclass0": "Alarm", "providerclass1": "Timer"})

            self.add_ipo("gong", "Gong", "gettext", "statusbar", None, True, [None, "g"])
        else:
            # if settings contains 'something', try to setup the IPOs:
            for name in data:
                if name != "DEFAULT":
                    ipodat = data[name]
                    hotkeys = []
                    n = 0
                    # catch "None"s
                    #print("NAME: ", name)
                    location = ipodat['location']
                    nonvis_location = ipodat['nonvis_location']
                    for t in [location, nonvis_location]:
                        if t == 'None':
                            t = None

                    # make an array of all hotkeys
                    while 'hotkey' + str(n) in ipodat:
                        hotkeys.append(ipodat['hotkey' + str(n)])
                        if hotkeys[-1] == 'None':
                            hotkeys[-1] = None
                        if hotkeys[-1] == '':       # There ARE no empty hotkeys...
                            hotkeys[-1] = ' '
                        n += 1
                    # setup the ipo.
                    self.add_ipo(ipodat['name'], ipodat['class_name'], ipodat['pollfct'], location, nonvis_location, ipodat.getboolean('vis'), hotkeys)
                    ipo_settings = {}
                    # now catch all keys that don't belong to the IAssambler object and pass it to the object itself.
                    for key in ipodat:
                        if key not in ("name", "class_name", "obj", "pollfct", "location", "nonvis_location", "vis", "hotkeys"):
                            ipo_settings[key] = ipodat[key]

                    # THIS COULD/SHOULD(?) BE PUSHED TO .add_ipo anyway
                    try:
                        # try to pass the settings to the newly created ipo
                        self.ipos[-1]['obj'].set_settings(ipo_settings)
                    except AttributeError:
                        # the ipo may not even have a set_settings Method!
                        pass



    def add_ipo(self, name, class_name, pollfct, location, nonvis_location, vis, hotkeys):
        # this creates the object and adds it to the IPOs array.
        self.ipos.append({"name": name, "class_name": class_name, "obj": globals()[class_name](), "pollfct": pollfct, "location": location, "nonvis_location": nonvis_location, "vis": vis, "hotkeys": hotkeys})
        self.ipos[-1]['obj'].vis(vis)

    def gettext(self):
        output = {"statusbar": "", "before_statusbar": "", "after_statusbar": "", "main": "", "infoline": ""}

        for ipo in self.ipos:
            if len(output["main"]) > 0:
                # add a return character if not added by the ipo itself.
                if output["main"][-2:] != "\r\n":
                    output["main"] += "\r\n"
            if len(output["statusbar"]) > 0:
                if output["statusbar"][-1] != " ":
                    output["statusbar"] += " "
            if len(output["infoline"]) > 0:
                if output["infoline"][-1] != " ":
                    output["infoline"] += " "

            if ipo["vis"]:
                text = getattr(ipo["obj"], ipo["pollfct"])()
                if len(text) > 0:
                    output[ipo["location"]] += text
            else:
                try:
                    # nonvis_location might not be set
                    text = getattr(ipo["obj"], ipo["pollfct"])()
                    if len(text) > 0:
                        output[ipo["nonvis_location"]] += text
                except:
                    pass

        text = output["before_statusbar"]
        if len(text) > 0:
            text += " "
        text += "[ " + output["statusbar"] + "] " + output["after_statusbar"] + "\r\n"
        text += output["main"]
        if len(output["infoline"]) > 0:
            text += output["infoline"]

        return text

    def get_help_text(self):
        helptext = "--  clock.py (v" + version() + ")  --  help  --\r\n"
        for ipo in self.ipos:
            try:
                text = ipo["obj"].help()
            except AttributeError:
                text = ""
            else:
                helptext += text
                if helptext[-1:] not in ["\r", "\n"]:
                    helptext += "\r\n"
        return helptext

    def input_key(self, char):
        sent = False
        double_hotkey_counter = 0
        if len(char) > 0:

            for key in self.hotkeys:
                # print("key: ", key, "ip: ", self.hotkeys[key]["obj"])
                if char == key[0]:
                    key[1]["obj"].pass_key(key[0])
                    sent = True
                elif key[0] is not None:
                    if char == key[0][0]:    # in this case it must be a double_hotkey [What? Why?]
                        double_hotkey_counter += 1

            # now handle double-hotkeys
            if not sent and double_hotkey_counter > 0:
                # get second character
                getch = pdp.Getch(1)
                print(char + ": ")
                input_char = getch()
                if len(input_char) > 0:
                    for key in self.hotkeys:
                            if char + input_char == key[0]:
                                key[1]["obj"].pass_key(key[0])
                                sent = True
                del getch

            if not sent:    # let's look if the char was a primary hotkey and toggle visibility
                for key in self.primary_hotkeys:
                    if char == key[0]:
                        ipo = key[1]
                        ipo["vis"] ^= True  # toggle visibility
                        ipo["obj"].vis(ipo["vis"])

    def refresh_hotkeys(self):
        """ # this prohibited multiply assigned hotkeys
        self.hotkeys = {}
        self.primary_hotkeys = {}
        for ipo in self.ipos:
            for key in ipo["hotkeys"]:
                if ipo["hotkeys"].index(key) == 0:
                    self.primary_hotkeys[key] = ipo
                else:
                    self.hotkeys[key] = ipo
        """

        self.hotkeys = []
        self.primary_hotkeys = []
        for ipo in self.ipos:
            for key in ipo["hotkeys"]:
                if ipo["hotkeys"].index(key) == 0:
                    # then its the primary hotkey
                    self.primary_hotkeys.append((key, ipo))
                else:
                    # add hotkey to hotkey list:
                    self.hotkeys.append((key, ipo))

    def save_settings(self):
        # uses a standardised get_settings/set_settings API of the IPOs. All settings
        # have to be handled as key/value pairs. Keys have to be unique! Every setting has to be a string.
        # Setting-arrays have to be broken up.
        config = configparser.ConfigParser()
        for ipo in self.ipos:
            name = ipo['name']
            config[name] = {'name': ipo['name'], 'class_name': ipo['class_name'], 'pollfct': ipo['pollfct'], 'location': str(ipo['location']), 'nonvis_location': str(ipo['nonvis_location']), 'vis': str(ipo['vis'])}
            n = 0
            for hotkey in ipo['hotkeys']:
                config[name]['hotkey' + str(n)] = str(hotkey)
                n += 1

            try:
                data = ipo['obj'].get_settings()
            except AttributeError:
                pass
            else:
                for key in data:
                    config[name][str(key)] = str(data[key])

        config_file = open(self.config_path, 'w')
        config.write(config_file)
        config_file.close()

        del config

    def load_settings(self):
        config = configparser.ConfigParser()
        config.read(self.config_path)
        # if file not readable or not exist, clear out config
        if len(config.sections()) == 0:
            del config
            config = {}
        return config

    def __del__(self):
        for ipo in self.ipos:
            try:
                del ipo["obj"]
            except:
                pass


#####
# MAIN FUNCTION

def main():
    quit = False

    # setting up signal catching (SIGINT, SIGTERM) to make sure changes are saved
    # when clock is exiting
    # TODO: This still does not save alarms if clock is not closed down in this way.
    # TODO    save should be triggered by messages or every x minutes...
    sig = pdp.SignalBundle([pdp.SignalCatcher(signal.SIGINT), pdp.SignalCatcher(signal.SIGTERM), pdp.SignalCatcher(signal.SIGILL)])

    # save terminal settings
    stty_settings = subprocess.run(["stty", "-g"], capture_output=True).stdout

    getch = pdp.Getch(1)
    ass = IAssembler()

    # repeat until quit is True or one of the signals SIGINT or SIGTERM are caught
    while not quit and not sig.getor():
        text = ass.gettext()
        pdp.clear()
        #print(text)
        # now sleep(/getch) for the rest of the second
        char = getch((1000000 - datetime.datetime.now().microsecond) / 1000000, prompt=text)
        # using prompt places the cursor behind the status bar. using the print() above will
        # place it below.
        if char in ["q", "Q"]:
            quit = True
        elif char in ["h"]:
            pdp.clear()
            # show help text and wait for a key for 7 secs (char is omitted!)
            print(ass.get_help_text())
            char = getch(7)
        else:
            ass.input_key(char)


    pdp.clear()
    print("quitting...")
    ass.save_settings()

    del ass
    # reset the console to default colors
    #subprocess.call(["printf", "\e[m"])

    # reset terminal to original settings
    subprocess.run(["stty", stty_settings.decode("utf-8")[:-1]])


if __name__ == "__main__":
    main()
