"""
INFORMATION PROVIDERS FOR THE CLOCK/Information Monitor

API description:

Every IPO class must have the following methods:

[.update()       TODO should this be callable from outside?? Currently called from within gettext()]
.gettext()  -> (multiline) string
.vis(boolean)
.pass_key(char)
.help()     -> (multiline) help text string
.get_settings()  -> dict
.set_settings(dict)

registered hotkeys (may be double-character!) will be passed to pass_key()
vis(boolean) is called to set/toggle visibility
set_settings(dict) is called to setup the IPO. all settings will be strings and have to be
  converted (if needed) by the IPO itself!
help() returns one or more help-text-lines. this should include hotkeys etc.
"""

import random
from multiprocessing import Process, Pipe  # for the gong sounds
import subprocess  # is this still needed?
import datetime
import gc
import os
import sys
import time
import pydpyl as pdp
import pydfl as pdf


#####
# INFORMATION PROVIDERS

class Clock:
    # Information provider for the time
    def __init__(self):
        self.visibility = True
        self.showseconds = False
        self.font = 0

        self.timestring = ""
        self.timebanner = ""

    def update(self):
        ctime = datetime.datetime.time(datetime.datetime.now())
        # make the ':' blink every second
        if ctime.second % 2 == 0:
            divider = ":"
        else:
            divider = " "
        # default font size is 0 (in pydpyl's bannerprint function)
        # create the timestring
        self.timestring = str(ctime.hour).zfill(2) + divider + str(ctime.minute).zfill(2)
        if self.showseconds:
            # add seconds only in non-default mode
            self.timestring += divider + str(ctime.second).zfill(2)

        self.timebanner = ""
        for l in pdf.banner(self.timestring, self.font):
            self.timebanner += l + "\r\n"

    def switchfont(self):
        if self.font == 0:
            self.font = 3
        else:
            self.font = 0

    def gettext(self):
        self.update()
        if self.visibility:
            return self.timebanner
        else:
            return self.timestring

    def vis(self, vis):
        self.visibility = vis

    def pass_key(self, char):
        if char in ["f"]:
            self.showseconds ^= True
        elif char in ["C"]:
            self.switchfont()

    def get_settings(self):
        settings = {}
        # collect parameters in a dictionary
        settings["font"] = self.font
        settings["visibility"] = self.visibility
        settings["showseconds"] = self.showseconds
        return settings

    def set_settings(self, settings={}):
        # set clock parameters here
        if "visibility" in settings:
            self.visibility = (settings["visibility"] == 'True')
        if "font" in settings:
            self.font = int(settings["font"])
        if "showseconds" in settings:
            self.showseconds = (settings["showseconds"] == 'True')

    def help(self):
        return "c/C   toggle clock/change clock's font size\r\nf     show/hide seconds"


###################################
class Alarm:
    def __init__(self):
        self.font = 3
        self.state = "clear"
        self.targettime = ""
        self.loudness = 0
        self.visibility = True
        self.silenced = False

    def update(self):
        # check if alarm should ring/visible alarm
        ctime = datetime.datetime.time(datetime.datetime.now())
        if self.state == "set" or self.state == "ringing":
            if (ctime.hour, ctime.minute) == (self.targettime[0], self.targettime[1]):
                if not self.silenced:
                    self.state = "ringing"
                else:
                    self.state = "set"
            else:
                self.silenced = False
                self.state = "set"

    def gettext(self):
        self.update()
        return_text = ""
        if self.state == "clear":
            # simple. no alarm -> no text
            if self.visibility:
                return_text = "no alarm"
            else:
                return_text = ""
        elif self.state == "set" or self.state == "ringing":
            ctime = datetime.datetime.time(datetime.datetime.now())
            timestring = "%1i:%02i" % (self.targettime[0], self.targettime[1])
            if (ctime.hour, ctime.minute) == (self.targettime[0], self.targettime[1]) and not self.silenced:
                if ctime.second % 2 == 0:
                    timestring = " " * len(timestring)
            if self.visibility:
                timebanner = "alarm:\r\n"
                for l in pdf.banner(timestring, self.font):
                    timebanner += l + "\r\n"
                return_text = timebanner
            else:
                return_text = "A:" + timestring

        return return_text

    def get_ringing(self):
        if self.state == "ringing":
            return True, ["bing2.wav", 0.25, "bing2.wav"]
        else:
            return False

    def switch_font(self):
        if self.font == 0:
            self.font = 3
        else:
            self.font = 0

    def set_alarm_time(self, setstring=""):
        """
        Set a new alarm time using input.

        :param setstring:
        :return:
        """

        if self.targettime != "":
            print("Please delete already set alarm/countdown with 'x' first!")
            time.sleep(1)
            return False
        if setstring == "":
            print("")
            inputstring = input("Enter the alarm time as h:mm:ss or h:mm : ")
        else:
            inputstring = setstring
        if len(inputstring) in (4, 5):
            inputstring += ":00"
        if len(inputstring) in (7, 8):
            # validity check of entered timestring
            check = 0
            try:
                if not int(inputstring[-2:]) in range(0, 60):
                    check += 1
                if not int(inputstring[-5:-3]) in range(0, 60):
                    check += 1
                if not int(inputstring[:-6]) in range(0, 24):
                    check += 1
                # not needed: if not inputstring[-3] + inputstring[-6] == "::":   check += 1
            except ValueError:
                check += 1

            if check == 0:  # All checks passed
                # alarmtime has the format =[h, m, s]
                self.targettime = (int(inputstring[:-6]), int(inputstring[-5:-3]), int(inputstring[-2:]))
                self.state = "set"
        else:
            self.targettime = ""
            self.state = "clear"

        return self.targettime != ""

    def vis(self, vis):
        self.visibility = vis

    def pass_key(self, char):
        if char in ["A"]:
            self.switch_font()
        elif char in ["sa"]:
            self.set_alarm_time()
        elif char in ["xa"]:
            self.targettime = ""
            self.state = "clear"
        elif char in [" "]:
            if self.state == "ringing":
                # self.state = "set"
                self.silenced = True

    def get_settings(self):
        settings = {}
        # collect parameters in a dictionary
        settings["font"] = self.font
        settings["state"] = self.state
        settings["targettime"] = self.targettime
        settings["loudness"] = self.loudness
        settings["visibility"] = self.visibility
        return settings

    def set_settings(self, settings={}):
        # set clock parameters here
        if "font" in settings:
            self.font = int(settings["font"])
        if "state" in settings:
            self.state = settings["state"]
        if "targettime" in settings:
            if settings["targettime"] != "":
                self.targettime = eval(settings["targettime"])
        if "loudness" in settings:
            self.loudness = settings["loudness"]
        if "visibility" in settings:
            self.visibility = settings["visibility"]

    def help(self):
        return "a/A   toggle alarm/change font size\r\nsa/xa set/delete alarm"


###################################
class Timer:
    def __init__(self):
        self.font = 0
        self.state = "clear"  # "clear" "set" "ringing"
        self.targettime = ""
        self.resttime = (0, 0, 0)
        self.loudness = 1
        self.visibility = True
        self.rest_time = (0, 0, 0)

    def update(self):
        # check if countdown reached 0
        if self.state in ["set"]:
            ctime = datetime.datetime.time(datetime.datetime.now())
            rest_time_secs = ((self.targettime[0] * 60 + self.targettime[1]) * 60 + self.targettime[2]) - (
                        (ctime.hour * 60 + ctime.minute) * 60 + ctime.second)
            if rest_time_secs <= 0:
                # self.rest_time = (0, 0, 0)
                self.state = "ringing"
            if rest_time_secs < -60:
                self.state = "clear"

            hour = int(rest_time_secs / 3600)
            rest_time_secs -= hour * 3600
            minute = int(rest_time_secs / 60)
            rest_time_secs -= minute * 60
            self.rest_time = (hour, minute, rest_time_secs)

    def gettext(self):
        self.update()
        if self.state in ["clear"]:
            if self.visibility:
                return "no timer"
            else:
                return ""
        elif self.state in ["set", "ringing"]:
            timestring = "%1i:%02i:%02i" % self.rest_time
            if self.state == "ringing":
                if abs(self.rest_time[2]) % 2 == 0:
                    # timestring = " " * len(timestring)
                    pass
                else:
                    # timestring = "0:00:00"
                    pass
            if self.visibility:
                timebanner = "timer:\r\n"
                for l in pdf.banner(timestring, self.font):
                    timebanner += l + "\r\n"
                return timebanner
            else:
                return "T:" + timestring
        else:
            return ""

    def get_ringing(self):
        if self.state == "ringing":
            return True, ["bing2.wav", 0.25, "bing2.wav"]
        else:
            return False

    def switch_font(self):
        if self.font == 0:
            self.font = 3
        else:
            self.font = 0

    def vis(self, vis):
        self.visibility = vis

    def set_alarm_time(self, setstring=""):
        """
        Set a new alarm time using input.

        :param setstring: a time string like '0:10'
        :return:
        """
        if self.state != "clear":
            print("Please delete already set alarm/countdown with 'x' first!")
            time.sleep(1)
            return False
        if setstring == "":
            print("")
            inputstring = input("Enter the countdown as h:mm:ss or m:ss or s : ")
        else:
            inputstring = setstring
        if len(inputstring) == 1: inputstring = "00:0" + inputstring
        if len(inputstring) == 2: inputstring = "00:" + inputstring
        if len(inputstring) == 4: inputstring = "0:0" + inputstring
        if len(inputstring) == 5: inputstring = "0:" + inputstring
        # TODO Timer and Alarm need better entry handling with regular expressions
        if len(inputstring) in (7, 8):
            # validity check of entered timestring
            check = 0
            try:
                if not int(inputstring[-2:]) in range(0, 100):       check += 1  # seconds
                if not int(inputstring[-5:-3]) in range(0, 100):     check += 1  # minutes
                if not int(inputstring[:-6]) in range(0, 100):        check += 1  # hours
                # not needed: if not inputstring[-3] + inputstring[-6] == "::":   check += 1
            except ValueError:
                check += 1

            if check == 0:  # All checks passed
                # alarmtime has the format =[h, m, s]
                ctime = datetime.datetime.time(datetime.datetime.now())

                alarmtime = [ctime.hour, ctime.minute, ctime.second]
                alarmtime[0] += int(inputstring[:-6])
                alarmtime[1] += int(inputstring[-5:-3])
                alarmtime[2] += int(inputstring[-2:])

                while alarmtime[2] > 60:
                    alarmtime[2] -= 60
                    alarmtime[1] += 1
                while alarmtime[1] > 60:
                    alarmtime[1] -= 60
                    alarmtime[0] += 1
                # while cdtime[0] < 0:
                #    cdtime[0] += 24
                self.targettime = (alarmtime[0], alarmtime[1], alarmtime[2])
                self.state = "set"
        else:
            self.targettime = ""
            self.state = "clear"

        return self.targettime != ""

    def pass_key(self, char):
        if char in ["T"]:
            self.switch_font()
        if char in ["st"]:
            self.set_alarm_time()
        if char in [" "]:
            if self.state == "ringing":
                self.state = "clear"
        if char in ["xt"]:
            self.targettime = ""
            self.state = "clear"

    def get_settings(self):
        settings = {}
        # collect parameters in a dictionary
        settings["font"] = self.font
        settings["targettime"] = self.targettime
        settings["state"] = self.state
        settings["loudness"] = self.loudness
        settings["visibility"] = self.visibility
        return settings

    def set_settings(self, settings={}):
        # set clock parameters here
        if "font" in settings:
            self.font = int(settings["font"])
        if "targettime" in settings:
            if settings["targettime"] != "":
                self.targettime = eval(settings["targettime"])
        if "loudness" in settings:
            self.loudness = settings["loudness"]
        if "state" in settings:
            self.state = settings["state"]
        if "visibility" in settings:
            self.visibility = settings["visibility"]

    def help(self):
        return "t/T   toggle timer/change timer font size\r\nst/xt set/delete timer"


###################################
class Netstat:
    # Network status monitor support
    def __init__(self, neturis=["localhost"]):
        self.allowed_non_uris = " '$-~=_+*#!%()/²³§?,;.:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZq@µ{}[]°^<>|"
        self.uris = neturis  # list of uri-s. if one uri-element itself is a list, usage is randomised (?)
        self.active = False
        self.startup()

    def startup(self):
        self.active = True
        self.stats = []  # list of process status
        self.pingprocs = []  # ping processes
        self.parent_conns = []  # ping process pipes (parent's ends)
        self.child_conns = []  # ping process pipes (child's ends)
        self.ping_running = []  # List of True/False if processes are pinging

        # setting up and starting the ping-worker processes:
        for uri in self.uris:
            if uri not in self.allowed_non_uris:
                p_c, c_c = Pipe()
                self.parent_conns.append(p_c)
                self.child_conns.append(c_c)
                self.pingprocs.append(Process(target=self.pingit, args=(c_c, uri)))
                self.pingprocs[-1].start()  # start the latest process
                self.stats.append(False)
                self.ping_running.append(False)
            else:
                self.pingprocs.append(None)
                self.parent_conns.append(None)
                self.child_conns.append(None)
                self.ping_running.append(False)
                self.stats.append(uri)
        self.lastrun = 1
        # self.dbg.add("Netstat/__init__: " + str(self.pingprocs))

    def dbg(self, msg):
        if not self.dbg == False:
            pass
            # dbg.add(msg)

    def kill(self):
        # end all the running processes
        for n in range(0, len(self.pingprocs)):
            if self.pingprocs[n] != None:
                self.parent_conns[n].send("quit")
        self.active = False

    def pingit(self, conn, uri):
        # a blocking process!
        # conn is the process pipe
        # uri is the uri to be used
        loopit = True
        lastrun = 0
        state = False
        while loopit:
            msg = conn.recv()  # get a message from "conn" pipe
            # if uri is a list, select next uri at random
            if isinstance(uri, (list, tuple)):
                ping_uri = uri[random.randint(0, len(uri) - 1)]
            else:
                ping_uri = uri

            if msg == "run":
                if lastrun + 30 <= time.time():
                    devnull = open(os.devnull, 'w')  # used to sink ping's stdout into
                    # now doing the pinging:
                    try:
                        ret = subprocess.call(["ping", "-c 1", "-t 100", ping_uri], stdout=devnull, stderr=devnull)
                    except:  # TODO do not use bare except, evaluate possible errorcodes
                        state = False
                        raise
                    else:
                        if ret == 0:  # if ping received echo
                            state = True
                        else:  # if ping did NOT receive an echo
                            state = False

                    devnull.close()  # close stdout sink

                    lastrun = time.time()
            elif msg == "quit":
                loopit = False

            # self.dbg.add("Netstat/pingit: " + str(ping_uri) + str(state))

            conn.send([ping_uri, state])

            # while conn.poll():      # flush massage pipe except for quit
            #    if conn.recv() == "quit":
            #        loopit = False
        conn.close()

    def get(self):
        # tries to get results from ping processes or tries to restart them
        for n in range(0, len(self.pingprocs)):
            if self.pingprocs[n] != None:
                if self.ping_running[n]:
                    if self.parent_conns[n].poll():
                        msg = self.parent_conns[n].recv()  # receives [uri, state]
                        self.stats[n] = msg[1]  # only the state
                        self.ping_running[n] = False  # set status of the proc to False
                else:
                    self.parent_conns[n].send("run")
                    self.ping_running[n] = True  # set status of the proc to True
            else:
                # old line --- self.stats[n] = self.stats[n]
                pass
        # self.stats contains all current stats
        # self.dbg.add("Netstat/get: " + str(self.stats))
        return self.stats

    def gettext(self):
        # build a netstat status text module from the stats
        self.get()
        text = "   {"
        for stat in self.stats:
            text += ""
            if str(stat) in self.allowed_non_uris:
                text += stat
            else:
                if stat:
                    text += "o"
                elif not stat:
                    text += ":"
        text += "}"
        return text

    def vis(self, vis):
        # currently does not care if it is visible
        if vis:
            if self.active:
                pass
            else:
                self.startup()
        else:
            if self.active:
                self.kill()
            else:
                pass

    def pass_key(self, char):
        if char in ["N"]:
            self.setup_menu()

    def get_settings(self):
        settings = {}
        # collect parameters in a dictionary
        settings["uris"] = self.uris
        settings["active"] = self.active
        return settings

    def set_settings(self, settings={}):
        # set clock parameters here
        if "uris" in settings:
            self.uris = eval(settings["uris"])
        if "active" in settings:
            self.active = (settings["active"] == 'True')
        self.kill()
        self.startup()

    def setup_menu(self):
        # text menu driven editing of the Netstat URIs.
        # TODO use the new prompt capability of Getch/getch to have proper input behavior
        char = "_"
        delete_it = False
        add_it = False
        edit_it = False
        getch = pdp.Getch(10)
        skipgetch = False
        itemname = ""
        prompt = ""

        while char != "" and char != "N":
            prompt = ""
            pdp.clear()
            print("NETSTAT SETUP")
            print("=============")
            # print(char)
            # print(ord(char))
            for n in range(0, len(self.uris)):
                print("  " + str(n + 1) + ":  " + self.uris[n])
            if delete_it + add_it + edit_it == 0:
                print("d: delete, a: add, e: edit, N: back")

            if char is chr(127) and len(itemname) > 0:  # BACKSPACE
                itemname = itemname[:-1]
            if char in "01234567890":
                itemname += char
            if char is chr(13) or char is chr(10):  # RETURN
                # Return AND Enter (cr and newline) are recognised
                if len(itemname) > 0:
                    itemnumber = int(itemname)
                    if itemnumber in range(0, len(self.uris) + 1):
                        if delete_it:
                            self.uris.pop(itemnumber - 1)
                        elif add_it:
                            newuri = input("New URI > ")
                            self.uris.insert(itemnumber, newuri)
                        elif edit_it:
                            print("old URI: " + self.uris[itemnumber - 1])
                            newuri = input("New URI > ")
                            self.uris.pop(itemnumber - 1)
                            self.uris.insert(itemnumber - 1, newuri)
                delete_it = add_it = edit_it = False
                itemname = ""
                skipgetch = True

            if char is "d" or delete_it:
                print("Enter item number to delete (x to cancel).")
                delete_it = True
                # print(itemname)
                prompt = itemname
            if char is "a" or add_it:
                print("Enter position to append item (0 for new first).")
                add_it = True
                # print(itemname)
                prompt = itemname
            if char is "e" or edit_it:
                print("Enter item number to edit. (x to cancel)")
                edit_it = True
                # print(itemname)
                prompt = itemname
            if char is "x":
                delete_it = add_it = edit_it = False
                skipgetch = False

            if skipgetch:
                char = "_"
                skipgetch = False
            else:
                char = getch(prompt=prompt + " ")

        # reinitiallise the Netstat object with the new URIs
        self.kill()
        self.startup()

    def __del__(self):
        self.kill()

    def help(self):
        return "n     toggle network status check\r\nN     edit network status targets"


###################################
class Gong:
    def __init__(self, gong="gong.wav", bing="bing.wav"):
        self.on = False
        self.gong = gong
        self.bing = bing
        self.os = os.name
        self.lastgong = (0, 0)

        if self.os == "posix":
            self.play = True
        else:
            self.play = False

        self.player = Player()

    def playthesignal(self, hour, minute):
        # sound the signal
        if hour > 12:
            hour -= 12
        if hour == 0:
            hour = 12

        # ring the hour
        for n in range(0, hour):
            self.player.playsound(self.gong)
            time.sleep(2)
        # take a break
        time.sleep(1.5)
        # ring the quarters
        for n in range(0, int(minute / 15)):
            self.player.playsound(self.bing)
            time.sleep(1)

    def update(self):
        if self.play:
            time = datetime.datetime.now()
            h = time.hour
            m = time.minute
            s = time.second  # for debugging
            if m % 15 == 0 and self.lastgong != (h, m) and self.on and self.play:
                self.lastgong = (h, m)
                # dbg.add("calling signal(" + str(h) + "," + str(m) + ")")

                # call playthesignal() as seperate thread
                p = Process(target=self.playthesignal, args=(h, m))
                p.start()

    def gettext(self):
        self.update()
        if self.on and self.play:
            return "◎ "
        else:
            return ""

    def vis(self, vis):
        # cannot be invisible
        pass

    def pass_key(self, char):
        if char in ["g"]:
            self.on ^= True

    def get_settings(self):
        settings = {}
        # collect parameters in a dictionary
        settings["on"] = self.on
        settings["gong"] = self.gong
        settings["bing"] = self.bing
        settings["lastgong"] = self.lastgong
        return settings

    def set_settings(self, settings={}):
        # set clock parameters here
        if "on" in settings:
            self.on = (settings["on"] == 'True')
        if "gong" in settings:
            self.gong = settings["gong"]
        if "bing" in settings:
            self.bing = settings["bing"]
        if "lastgong" in settings:
            self.lastgong = eval(settings["lastgong"])

    def help(self):
        return "g     toggle audible quarter hourly gong"


###################################
class Beeper:
    """Cares for audible alarms, timers and similar.
    This has one output: the audible alarm signal ((*))

    An object using Beeper must have the method get_ringing(), returning either False or True.
    A second return value may be the desired sound file name.
    """

    def __init__(self):
        self.providerclasses = []
        self.providers = []
        self.player = Player()
        self.default_sound = "bing2.wav"
        self.on = True
        self.autounmute = True
        # TODO use "amixer set Master unmute/mute" to unmute/mute for alarms! gong should respect system settings
        #  also: maybe fade in the alarm? might be a good idea

    def gettext(self):
        if self.on:
            for obj in self.providers:
                result = obj.get_ringing()
                # if len(result) > 0:
                if isinstance(result, (list, tuple)):
                    if result[0]:
                        if len(result[1]) > 1:
                            if self.autounmute:
                                os.system('amixer --quiet set Master unmute')
                            self.player.playsound(result[1].pop(), 0)
                            while len(result[1]) > 0:
                                delay = result[1].pop()
                                self.player.playsound(result[1].pop(), delay)
                        else:
                            self.player.playsound(result[1])
                else:
                    if result:
                        self.player.playsound(self.default_sound)
        else:
            # os.system('amixer set Master mute')
            pass
            # TODO ONLY IF it was muted before !!
        if self.on:
            return "((*))"
        else:
            return ""

    def get_settings(self):
        n = 0
        settings = {}
        for p in self.providerclasses:
            settings['providerclass' + str(n)] = p
            n += 1
        settings['on'] = self.on
        settings['autounmute'] = self.autounmute
        return settings

    def set_settings(self, data):
        for key in data:
            if "providerclass" in key:
                self.providerclasses.append(data[key])
            if "on" == key:
                self.on = (data[key] == 'True')
            if "autounmute" == key:
                self.autounmute = (data[key] == 'True')
        self.update_providers()

    def update_providers(self):
        for p in self.providerclasses:
            for obj in gc.get_objects():
                if isinstance(obj, globals()[p]):
                    self.providers.append(obj)

    def vis(self, vis):
        # add a visible alarm (blinking bar or similar)
        pass

    def pass_key(self, char):
        if char == "b":
            self.on ^= True


###################################
class Color:
    def __init__(self):
        self.color_list = ["31", "32", "33", "34", "35", "36"]
        self.color = 0

    def gettext(self):
        self.set_color()
        return ''

    def set_color(self):
        color_code = "\e[1;" + self.color_list[self.color] + "m"
        subprocess.call(["printf", "\e[m"])
        subprocess.call(["printf", color_code])

    def switch_color(self):
        self.color = (self.color + 1) % len(self.color_list)
        # self.set_color()
        # return newcolor

    def vis(self, vis):
        pass

    def get_settings(self):
        return {'color': self.color}

    def set_settings(self, settings):
        if 'color' in settings:
            self.color = int(settings['color'])

    def pass_key(self, char):
        if char == "#":
            self.switch_color()

    def __del__(self):
        subprocess.call(["printf", "\e[m"])

    def help(self):
        return "b     toggle audible alarm"


###################################
# HELPER OBJECTS
class Player:
    """Plays wave files placed in ./RSC/

    This is a non-blocking player!

    USAGE:
    .playsound(sound)
      sound is the name of a wave file ending
    """

    def __init__(self):
        self.path = "./RSC/audio/"
        self.os = os.name

    def playsound(self, sound="gong.wav", delay=0):
        # replace all this with mplayer call
        soundfile = self.path + sound
        if self.os == "posix":
            # === LINUX SOLUTION
            # this would be quite native:
            # os.system("aplay " + tonfile)

            # this uses mplayer to allow volume control in the future
            # with open(os.devnull, "w") as f:
            #    subprocess.Popen(
            #        ["mplayer", "-really-quiet", "-msglevel", "all=-1", "-nolirc", soundfile],
            #        stdout=f
            #    )
            try:
                Process(target=self._call_player, args=(soundfile, delay)).start()
            except Exception as e:
                print("\a")
                print("Unexpected Error:", sys.exc_info()[0])
        else:
            print("\a")

    def _call_player(self, soundfile, delay):
        sys.stdout = os.devnull
        time.sleep(delay)
        with open(os.devnull, "w") as f:
            #subprocess.Popen(
            #    ["mplayer", "-quiet", "-msglevel", "all=-1", "-nolirc", soundfile],
            #    stdout=f
            #)
            subprocess.Popen( ["aplay", "--quiet", soundfile] )


if __name__ == "__main__":
    print("This is the IPO library of the clock.py project.")
    print("This library is not meant to be run.")
    print("")
